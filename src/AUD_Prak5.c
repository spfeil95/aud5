/*
 ============================================================================
 F Name        : AUD_Prak5.c
 Author      : Steffen Pfeil
 Version     :
 Copyright   : (C) 2016 - Steffen Pfeil
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

#define BUFFERSIZE 128
#define q 33554393
#define d 32

int steps = 0;
int skip [UCHAR_MAX];

int myindex(char c) {
	return (unsigned char) c;
}

void initskip(char *p) {
	int j, M = strlen(p);
	for (j = 0; j < UCHAR_MAX; j++) skip[j] = M;
	for (j = 0; j < M; j++) skip[myindex(p[j])] = M-j-1;
}

/*suche in a nach p*/
int mischarsearch(char *p, char *a) {
	int i, j, t, M = strlen(p), N = strlen(a);
	initskip(p);
	for (i = M - 1, j = M - 1; j >= 0; i--, j--)
		while (a[i] != p[j]) {
			steps++;
			t = skip[myindex(a[i])];
			i += (M - j > t) ? M - j : t;
			/* Springe um skip, mindestens aber um 1 nach rechts */
			if (i >= N)
				return N + 1;
			j = M - 1; /* Setze Musterzeiger ganz nach rechts */
		}
	return i + 1;
}

int rksearch(char *p, char *a) {
	int i, dM = 1, h1 = 0, h2 = 0, M = strlen(p), N = strlen(a);
	for (i = 1; i < M; i++)
		dM = (d * dM) % q;
	for (i = 0; i < M; i++) {
		h1 = (h1 * d + myindex(p[i])) % q;
		h2 = (h2 * d + myindex(a[i])) % q;
	}
	for (i = 0; h1 != h2; i++) {
		steps++;
		h2 = (h2 + d * q - myindex(a[i]) * dM) % q;
		/* Summand d*q zur Sicherstellung, dass h2 > 0 */
		h2 = (h2 * d + myindex(a[i + M])) % q;
		if (i > N - M)
			return N;
	}
	return i;
}


int show_menue() {

	int choice;
	printf("Algorithmus wählen: \n");
	printf("------------------------------------\n\n");
	printf("(1) Mischasearch\n");
	printf("(2) Rabin-Karb\n");
	printf("(3) Beenden\n\n");
	printf("------------------------------------\n");
	printf("Auswahl eigeben:");
	scanf("%d", &choice);
	return choice;

}


int main(void) {

	int selectionCode = 0;
	char text[100] = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam";
	char* searchStr = malloc(sizeof(char) * 1024);

	do {

		selectionCode = show_menue();

		if(selectionCode == 1){

			printf("Suchwort eingeben: ");
			scanf("%s", searchStr);
			printf("\n\nSuche mit Mischasearch nach String '%s'\n", searchStr);
			int res = mischarsearch(searchStr, text);
			if(res < strlen(text)){

				printf(">> String '%s' gefunden an Stelle %d. %d Schritte\n\n\n", searchStr, res, steps);

			}else{

				printf(">> String '%s' nicht gefunden\n\n\n", searchStr);

			}
			printf("Weiter mit enter...\n\n");

		}else if(selectionCode == 2){


			printf("Suchwort eingeben: ");
			scanf("%s", searchStr);
			printf("\n\nSuche mit Rabin-Karb nach String '%s'\n", searchStr);
			int res = rksearch(searchStr, text);
			if(res < strlen(text)){

				printf(">> String '%s' gefunden an Stelle %d. %d Schritte\n\n\n", searchStr, res, steps);

			}else{

				printf(">> String '%s' nicht gefunden\n\n\n", searchStr);

			}
			printf("Weiter mit enter...\n\n");

		}

	} while (selectionCode != 3);

	return EXIT_SUCCESS;

}
